<!-- modal large -->
<div class="modal fade" id="create_view_modal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Viewer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="create_view_modal_content" class="modal-body">
                <img id="create_view_modal_wait" src="/images/icon/loading.gif" class="rounded mx-auto d-block" alt="loading...">
                <!-- Content goes in here -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" name="create_view_modal_submit" id="create_view_modal_submit" class="btn btn-primary" style="display: none;">Confirm</button>
            </div>
        </div>
    </div>
    <input type="hidden" id="create_view_modal_referral" value="">
</div>
<!-- end modal large -->