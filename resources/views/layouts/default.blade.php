<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="name" content="Mantra">
    <meta name="description" content="Making Business Project Management Easier">
    <meta name="author" content="UPOU IS226 Students (Libiano & Capispisan)">
    <meta name="keywords" content="mantra project-management">
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Title Page-->
    <title>Mantra</title>

    <!-- Fontfaces CSS-->
    <link href="{!! asset('css/font-face.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/font-awesome-4.7/css/font-awesome.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/font-awesome-5/css/fontawesome-all.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/mdi-font/css/material-design-iconic-font.min.css') !!}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{!! asset('vendor/bootstrap-4.1/bootstrap.min.css') !!}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{!! asset('vendor/animsition/animsition.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/wow/animate.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/css-hamburgers/hamburgers.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/slick/slick.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/select2/select2.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/perfect-scrollbar/perfect-scrollbar.css') !!}" rel="stylesheet" media="all">
    <!-- <link href="{!! asset('/vendor/swatkins/gantt/css/gantt.css') !!}" rel="stylesheet" type="text/css"> -->
    <!-- <link href="{!! asset('vendor/bootstrap-datetime-picker/css/bootstrap-theme.min.css') !!}" rel="stylesheet" media="all"> -->
    <!-- <link href="{!! asset('vendor/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css') !!}" rel="stylesheet" media="all"> -->
    <!-- <link href="{!! asset('vendor/bootstrap-datetime-picker/css/bootstrap.min.css') !!}" rel="stylesheet" media="all"> -->
    @toastr_css

    <!-- Main CSS-->
    <link href="{!! asset('css/theme.css') !!}" rel="stylesheet" media="all">

    <!-- Jquery JS-->
    <script src="{!! asset('vendor/jquery-3.2.1.min.js') !!}"></script>
</head>
<body class="animsition">
    <div class="page-wrapper" id="app">
        <!-- Navigation -->
        @include('layouts.header')
        <div class="page-content--bgf7">
            @yield('content')

            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2019 Francis Libiano. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- /#page-wrapper -->
   </div>

   @include('modals.template')

    <!-- Bootstrap JS-->
    <script src="{!! asset('vendor/bootstrap-4.1/popper.min.js') !!}"></script>
    <script src="{!! asset('vendor/bootstrap-4.1/bootstrap.min.js') !!}"></script>

    <!-- Vendor JS --> 
    <script src="{!! asset('vendor/slick/slick.min.js') !!}"></script>
    <script src="{!! asset('vendor/wow/wow.min.js') !!}"></script>
    <script src="{!! asset('vendor/animsition/animsition.min.js') !!}"></script>
    <script src="{!! asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}"></script>
    <script src="{!! asset('vendor/counter-up/jquery.waypoints.min.js') !!}"></script>
    <script src="{!! asset('vendor/counter-up/jquery.counterup.min.js') !!}"></script>
    <script src="{!! asset('vendor/circle-progress/circle-progress.min.js') !!}"></script>
    <script src="{!! asset('vendor/perfect-scrollbar/perfect-scrollbar.js') !!}"></script>
    <script src="{!! asset('vendor/chartjs/Chart.bundle.min.js') !!}"></script>
    <script src="{!! asset('vendor/select2/select2.min.js') !!}"></script>
    <!-- <script src="{!! asset('vendor/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js') !!}"></script> -->

    @toastr_js
    @toastr_render

    <!-- Main JS-->
    
    <script src="{!! asset('js/main.js') !!}"></script>
    <script src="{!! asset('js/app.js') !!}"></script>
    <script src="{!! asset('js/opensms.js') !!}"></script>

</body>
</html>