@extends('layouts.default')

@section('content')
<!-- BREADCRUMB-->
<section class="au-breadcrumb2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="au-breadcrumb-content">
                    <div class="au-breadcrumb-left">
                        <span class="au-breadcrumb-span">You are here:</span>
                        <ul class="list-unstyled list-inline au-breadcrumb__list">
                            <li class="list-inline-item active">
                                <a href="#">Home</a>
                            </li>
                            <li class="list-inline-item seprate">
                                <span>/</span>
                            </li>
                            <li class="list-inline-item">Dashboard</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END BREADCRUMB-->

<section>
    <div class="container">
        <div class="row" id="app"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Send Notification</div>

                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="mobile_number" class=" form-control-label">Mobile No.</label>
                                        <input type="text" id="mobile_number" placeholder="Enter Mobile No.." class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label for="message" class=" form-control-label">Message</label>
                                        <input type="text" id="message" placeholder="Enter Notification Message.." class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card-footer">
                        <button id="send" type="button" class="btn btn-outline-primary btn-sm" onclick="sendMessage('{{ url('/sms') }}');">
                        <i class="fa fa-location-arrow"></i>&nbsp; Send Notification</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="p-t-20">
    <sms-sent></sms-sent>
    <br>
</section>

<section class="p-t-20">
    <passport-clients></passport-clients>
    <br/>
</section>
@endsection
