<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="name" content="CMSC207 :: Open SMS">
    <meta name="description" content="An open source Notification System Solution by Francis Libiano">
    <meta name="author" content="Francis R. Libiano">
    <meta name="keywords" content="opensms cmsc207exam exam">

    <title>OpenSMS</title>

    <!-- Fontfaces CSS-->
    <link href="{!! asset('css/font-face.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/font-awesome-4.7/css/font-awesome.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/font-awesome-5/css/fontawesome-all.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! asset('vendor/mdi-font/css/material-design-iconic-font.min.css') !!}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{!! asset('vendor/bootstrap-4.1/bootstrap.min.css') !!}" rel="stylesheet" media="all">

    <!-- Custom fonts for this template -->
    <!-- <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{!! asset('css/grayscale.min.css') !!}" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Navigation -->
    @if (Route::has('login'))
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">Open SMS</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    @auth
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/home') }}">{{ Auth::user()->name }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/home') }}">Home</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#" onclick="create_view_modal('{{ url('login/modal') }}', '');">Login</a>
                    </li>
                      @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link js-scroll-trigger" href="#" onclick="create_view_modal('{{ url('register/modal') }}', '');">Register</a>
                      </li>
                      @endif
                    @endauth
                </ul>
            </div>
        </div>
    </nav>
    @endif

    <!-- Header -->
    <header class="masthead">
        <div class="container d-flex h-100 align-items-center">
            <div class="mx-auto text-center">
                <h1 class="mx-auto my-0 text-uppercase">OpenSMS</h1>
                <h2 class="text-white-50 mx-auto mt-2 mb-5">A Low Cost, Low Budget SMS Notification Solution for System Administrators</h2>
                @auth
                <a class="btn btn-primary js-scroll-trigger" href="{{ url('/home') }}">Get Started</a>
                @else
                <a class="btn btn-primary js-scroll-trigger" href="#" onclick="create_view_modal('{{ url('login/modal') }}', '');">Get Started</a>
                @endauth
            </div>
        </div>
    </header>

    <!-- Contact Section -->
    <section class="contact-section bg-black">
        <div class="container">

            <div class="row">

                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <i class="fas fa-map-marked-alt text-primary mb-2"></i>
                            <h4 class="text-uppercase m-0">Subject</h4>
                            <hr class="my-4">
                            <div class="small text-black-50">Project in CMSC 207</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <i class="fas fa-envelope text-primary mb-2"></i>
                            <h4 class="text-uppercase m-0">Email</h4>
                            <hr class="my-4">
                            <div class="small text-black-50">
                                <a href="#">frlibiano@up.edu.ph</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <i class="fas fa-mobile-alt text-primary mb-2"></i>
                            <h4 class="text-uppercase m-0">Submitted to</h4>
                            <hr class="my-4">
                            <div class="small text-black-50">Prof. Gulshan Vasson</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="social d-flex justify-content-center">
                <a href="https://www.facebook.com/FallenKNight85" class="mx-2">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="#" class="mx-2">
                    <i class="fab fa-github"></i>
                </a>
                <a href="#" class="mx-2">
                    <i class="fas fa-envelope"></i>
                </a>
            </div>

        </div>
    </section>

    <!-- Footer -->
    <footer class="bg-black small text-center text-white-50">
        <div class="container">
            Copyright &copy; OpenSMS 2019
        </div>
    </footer>

    @include('modals.template')

    <!-- Jquery JS-->
    <script src="{!! asset('vendor/jquery-3.2.1.min.js') !!}"></script>
    
    <!-- Bootstrap JS-->
    <!-- <script src="{!! asset('vendor/bootstrap-4.1/popper.min.js') !!}"></script> -->
    <script src="{!! asset('vendor/bootstrap-4.1/bootstrap.min.js') !!}"></script>

    <!-- Plugin JavaScript -->
    <!-- <script src="vendor/jquery-easing/jquery.easing.min.js"></script> -->
    <script src="{!! asset('vendor/jquery-easing/jquery.easing.min.js') !!}"></script>

    <!-- Custom scripts for this template -->
    <script src="{!! asset('js/grayscale.min.js') !!}"></script>
    <script src="{!! asset('js/opensms.js') !!}"></script>

    @toastr_js
    @toastr_render

</body>

</html>
