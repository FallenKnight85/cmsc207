<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Item extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'description'];

}
