<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $mobile_number
 * @property string $message
 * @property string $sent_by
 * @property string $sendStatus
 * @property string $created_at
 * @property string $updated_at
 */
class Sms extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['mobile_number', 'message', 'sent_by', 'sendStatus'];

}
