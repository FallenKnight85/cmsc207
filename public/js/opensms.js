$('#create_view_modal').on('hidden.bs.modal', function () {
    $('#create_view_modal_content').html('<img id="create_view_modal_wait" src="/images/icon/loading.gif" class="rounded mx-auto d-block" alt="loading...">');
    $('#create_view_modal_submit').hide();
    $(".modal-dialog").removeClass("modal-xl").addClass("modal-lg");
    $('#create_view_modal_submit').unbind();
});

const create_view_modal = (link, ref) => {
    console.log('Modal Created for Link: ' + link);
    $("#create_view_modal_wait").show();
    $('#create_view_modal').modal('show');

    $(document).ajaxComplete(function () {
        $("#create_view_modal_wait").hide();
    });

    $('#create_view_modal').find('#create_view_modal_referral').val(ref);
    $('#create_view_modal_content').load(link);
}

const sendMessage = (uri) => {
    if (confirm("Are you sure you want to send notification?")) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
       
        $.ajax({
            url: uri,
            method: "POST",
            data: {
                mobile_number:  $('#mobile_number').val(),
                message: $('#message').val()
            },
            success: function (result) {
                toastr.success("Notification places in queue", "Success");
            },
            error: function(xhr, error){
                console.debug(xhr); console.debug(error);
                toastr.error(error.message, "Failed");
            }
        });
    }
    $('#mobile_number').val('');
    $('#message').val('');
}

const navigate = (link) => {
    console.log('navigate to ' + link);
    // Avoid the link click from loading a new page
    //event.preventDefault();
    // $('.page-container').html('<img id="nav_wait" src="/images/icon/loading.gif" class="rounded mx-auto d-block" alt="loading...">'); 
    // $("#nav_wait").show();

    // $(document).ajaxComplete(function(){
    //   $("#nav_wait").hide();
    // });

    // Load the content from the link's href
    $('.main-content').remove();
    $('.account-item').removeClass('show-dropdown');
    $('.page-container').load(link);
}
